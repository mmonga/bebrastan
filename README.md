This repository collects code and data used to analyze the results of
the 2014 edition of the
[Italian Bebras/Kangourou contest](http://kangourou.di.unimi.it/),
exploiting the
[Item Response Theory](https://en.wikipedia.org/wiki/Item_response_theory)
statistical methodology in order to infer the difficulty of each of
the proposed tasks starting from the scores attained by the
participants.

# How to use it #

## Prerequisites ##

* GNU/Linux
* GNU make
* [Stan](http://mc-stan.org/) (tested with version 2.5.0)
* Python
* Pictures were produced with IPython + Matplotlib.

## Instructions ##

In the `Makefile` change the variable `STANDIR` according to your
installation of Stan. Now a `make all` should reproduce the analyses.

## Files ##

* `README.md`: this file
* `Makefile`
* `irt-small.stan`: Stan model, "small" version
* `irt_small_model.py`: PyStan model initialization 
* `kangourou-small.data.R`: data from Kangourou
* `irt-small-optimize.py`: Python driver for optimization
* `irt-small.py`: Python driver for sampling
* `irt-big.stan`: Stan model, "big" version
* `irt_big_model.py`: PyStan model initialization 
* `kangourou-big.data.R`: data from Kangourou
* `irt-big-optimize.py`: Python driver for optimization
* `irt-big.py`: Python driver for sampling
* `kangourou-reg.data.R`: data by region
* `kangourou-school.data.R`: data by school
* `read.py`: Python data parsing module
* `An IRT analysis of Kangourou results.ipynb`: IPython notebook

The IPython notebook can be viewed
[here](http://nbviewer.ipython.org/urls/bitbucket.org/mmonga/bebrastan/raw/0ba9291da55e073222d327b098a0f6fb49578c30/An%20IRT%20analysis%20of%20Kangourou%20results.ipynb)


# Reference #

Results are discussed in "Bellettini, Lonati, Malchiodi, Monga,
Morpurgo, Torelli *How challenging are Bebras tasks? An IRT analysis
based on the performance of Italian students*, ITiCse 2015".









