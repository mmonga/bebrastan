import irt_big_model as irt

for i in xrange(4):
    opt = irt.model.optimizing(data=irt.obs, init=0)
    irt.pystan.stan_rdump(opt, 'irt-big-opt-{}.R'.format(i+1))
