data {
    int<lower=1> K; // number of categories
    int<lower=1> Q; // number of quizzes
    int<lower=1> N; // number of records
    int<lower=0,upper=1> y[N];  // results
    int<lower=1,upper=K> ii[N]; // category for y[n]
    int<lower=1,upper=Q> jj[N]; // quiz for y[n]
    int<lower=1, upper=K> kk[Q]; // category of a quiz
    real<lower=-1, upper=1> m_step[Q];
    real<lower=0> b[Q]; // wrong answers for multiple choices
}

parameters {
    vector[K] theta_raw;           // ability
    vector[Q] delta_raw;           // difficulty
    vector[Q] alpha_raw;           // discrimination
    real<lower=0, upper=1> eta[Q]; // guessing
    
    real<lower=0, upper=pi()/2> s_theta_unif;
    real<lower=0, upper=pi()/2> s_delta_unif;
    real<lower=0, upper=pi()/2> s_alpha_unif;
    real<lower=-pi()/2, upper=pi()/2> m_delta_unif[Q];
}

transformed parameters {
    vector[K] theta;               // ability
    vector[Q] delta;               // difficulty
    vector[Q] alpha;               // discrimination
    real<lower=0> s_theta;
    real<lower=0> s_delta;
    real<lower=0> s_alpha;
    vector[Q] m_delta;
    
    // reparameterization (see Stan manual, chapter 19)        
    s_theta <- 5*tan(s_theta_unif); // faster than s_theta ~ cauchy(0, 5);
    s_delta <- 5*tan(s_delta_unif); // faster than s_delta ~ cauchy(0, 5);
    s_alpha <- 5*tan(s_alpha_unif); // faster than s_alpha ~ cauchy(0, 5);

    theta <- s_theta * theta_raw; // faster than theta ~ normal(0, s_theta);
    alpha <- s_alpha * alpha_raw; // faster than alpha ~ normal(0, s_alpha);
    
    for (j in 1:Q) {
        // faster than m_delta ~ cauchy(m_step, .5);
        m_delta[j] <- m_step[j] + 0.5 * tan(m_delta_unif[j]);
        // faster than delta ~ normal(m_delta, s_delta);
        delta[j] <- m_delta[j] + s_delta * delta_raw[j];
    }
}

model {
    vector[N] p;	

    for (n in 1:N) {
        p[n] <- eta[jj[n]] + (1 - eta[jj[n]]) *
            inv_logit(exp(alpha[jj[n]]) *
                      (theta[ii[n]] - delta[jj[n]]));
   }

    theta_raw ~ normal(0, 1);
    alpha_raw ~ normal(0, 1);
    delta_raw ~ normal(0, 1);
    eta ~ beta(1, b);
    y ~ bernoulli(p);
}

generated quantities {
    vector[Q] diff;
    for (j in 1:Q) 
        diff[j] <- theta[kk[j]] - delta[j];
}
