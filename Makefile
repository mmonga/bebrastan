STANDIR=../cmdstan.git
DIR=$(shell cd $(STANDIR); echo $$OLDPWD)
SEED=424242
SAMPLES=5000
WARMUP=5000
THIN=2

SMALL=irt-small-1.csv irt-small-2.csv irt-small-3.csv irt-small-4.csv
SMALL_OPT=$(subst csv,R,$(subst small,small-opt,$(SMALL)))
BIG=$(subst small,big,$(SMALL))
BIG_OPT=$(subst small,big,$(SMALL_OPT))
SCHOOL=$(subst small,school,$(SMALL))
SCHOOL_OPT=$(subst small,school,$(SMALL_OPT))
REG=$(subst small,reg,$(SMALL))
REG_OPT=$(subst small,reg,$(SMALL_OPT))



all: small big school reg

small: $(SMALL)

big: $(BIG)

school: $(SCHOOL)

reg: $(REG)


$(SMALL): kangourou-small.data.R irt-small-opt-?.R irt-small
	./irt-small sample num_samples=$(SAMPLES) \
                           num_warmup=$(WARMUP) thin=$(THIN) \
		           init=$(subst csv,R,$(subst small,small-opt,$@)) \
			   id=$(shell echo $@ | cut -d- -f3 | cut -d. -f1) \
		    random seed=$(SEED) \
		    data file=kangourou-small.data.R \
		    output file=$@ diagnostic_file=diag-$@

$(BIG): kangourou-big.data.R irt-big-opt-?.R irt-big
	./irt-big sample num_samples=$(SAMPLES) \
                           num_warmup=$(WARMUP) thin=$(THIN) \
		           init=$(subst csv,R,$(subst big,big-opt,$@)) \
			   id=$(shell echo $@ | cut -d- -f3 | cut -d. -f1) \
		    random seed=$(SEED) \
		    data file=kangourou-big.data.R \
		    output file=$@ diagnostic_file=diag-$@

$(SCHOOL): kangourou-school.data.R irt-school-opt-?.R irt-school
	./irt-school sample num_samples=$(SAMPLES) \
                           num_warmup=$(WARMUP) thin=$(THIN) \
		           init=$(subst csv,R,$(subst school,school-opt,$@)) \
			   id=$(shell echo $@ | cut -d- -f3 | cut -d. -f1) \
		    random seed=$(SEED) \
		    data file=kangourou-school.data.R \
		    output file=$@ diagnostic_file=diag-$@

$(REG): kangourou-reg.data.R irt-reg-opt-?.R irt-reg
	./irt-reg sample num_samples=$(SAMPLES) \
                           num_warmup=$(WARMUP) thin=$(THIN) \
		           init=$(subst csv,R,$(subst reg,reg-opt,$@)) \
			   id=$(shell echo $@ | cut -d- -f3 | cut -d. -f1) \
		    random seed=$(SEED) \
		    data file=kangourou-reg.data.R \
		    output file=$@ diagnostic_file=diag-$@


kangourou-%.data.R: irt_%_model.py rank.csv
	python -OO $<

irt-small-opt-%.R : irt-small-optimize.py
	python -OO $<

irt-big-opt-%.R : irt-big-optimize.py
	python -OO $<

irt-school-opt-%.R : irt-school-optimize.py
	python -OO $<

irt-reg-opt-%.R : irt-reg-optimize.py
	python -OO $<


irt-small irt-big irt-school irt-reg: irt-%: irt-%.stan
	$(MAKE) -C $(STANDIR) $(DIR)/$@

.PHONY: clean-small clean-big clean-school clean-reg


clean-small:
	rm -f $(SMALL)  $(SMALL_OPT) irt-small

clean-big:
	rm -f $(BIG)  $(BIG_OPT) irt-big

clean-school:
	rm -f $(SCHOOL)  $(SCHOOL_OPT) irt-school

clean-reg:
	rm -f $(REG)  $(REG_OPT) irt-reg


clean: clean-small clean-big clean-school clean-reg
