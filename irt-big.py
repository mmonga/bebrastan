import numpy as np
import irt_big_model as irt

opts = [irt.model.optimizing(data=irt.obs) for i in xrange(4)]
fit = irt.model.sampling(data=irt.obs, init=opts,
                         iter=10000,
                         thin=2,
                         chains=4)
print fit
d = fit.extract()

np.savez('da-3PL-big.npz',
         delta=d['delta'], alpha=d['alpha'], eta=d['eta'],
         diff=d['diff'])

import pickle
pickle.dump(fit, open('fit-3PL-big.pkl', 'w'))
pickle.dump(opts[0], open('opt-3PL-big.pkl', 'w'))
