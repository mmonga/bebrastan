import numpy as np
import irt_small_model as irt

opts = [irt.model.optimizing(data=irt.obs, init=0) for i in xrange(4)]
fit = irt.model.sampling(data=irt.obs, init=opts,
                         iter=10000,
                         thin=2,
                         seed=424242,
                         sample_file='prova-small',
                         chains=4)
print fit
d = fit.extract()

np.savez('da-3PL-small.npz',
         delta=d['delta'], alpha=d['alpha'], eta=d['eta'],
         diff=d['diff'])

import pickle
pickle.dump(fit, open('fit-3PL-small.pkl', 'w'))
pickle.dump(opts[0], open('opt-3PL-small.pkl', 'w'))
