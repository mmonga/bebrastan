data {
    int<lower=1> T; // number of teams
    int<lower=1> K; // number of categories
    int<lower=1> Q; // number of quizzes
    int<lower=1> N; // number of records
    int<lower=0,upper=1> y[N];  // results
    int<lower=1,upper=T> ii[N]; // team for y[n]
    int<lower=1,upper=Q> jj[N]; // quiz for y[n]
    int<lower=1, upper=K> kq[Q]; // category of a quiz
    int<lower=1, upper=K> kt[T]; // category of a team
    real<lower=-1, upper=1> m_step[Q];
    real<lower=0> b[Q]; // wrong answers for multiple choices
}

parameters {
    vector[T] theta_raw;           // ability
    vector[Q] delta_raw;           // difficulty
    vector[Q] alpha_raw;           // discrimination
    real<lower=0, upper=1> eta[Q]; // guessing
    
    real<lower=0, upper=pi()/2> s_theta_unif;
    real<lower=0, upper=pi()/2> s_delta_unif;
    real<lower=0, upper=pi()/2> s_alpha_unif;
    real<lower=-pi()/2, upper=pi()/2> m_delta_unif[Q];
}

transformed parameters {
    vector[T] theta;               // ability
    vector[Q] delta;               // difficulty
    vector[Q] alpha;               // discrimination
    real<lower=0> s_theta;
    real<lower=0> s_delta;
    real<lower=0> s_alpha;
    vector[Q] m_delta;
    
    // reparameterization (see Stan manual, chapter 19)        
    s_theta <- 5*tan(s_theta_unif); // faster than s_theta ~ cauchy(0, 5);
    s_delta <- 5*tan(s_delta_unif); // faster than s_delta ~ cauchy(0, 5);
    s_alpha <- 5*tan(s_alpha_unif); // faster than s_alpha ~ cauchy(0, 5);

    theta <- s_theta * theta_raw; // faster than theta ~ normal(0, s_theta);
    alpha <- s_alpha * alpha_raw; // faster than alpha ~ normal(0, s_alpha);
    
    for (j in 1:Q) {
        // faster than m_delta ~ cauchy(m_step, .5);
        m_delta[j] <- m_step[j] + 0.5 * tan(m_delta_unif[j]);
        // faster than delta ~ normal(m_delta, s_delta);
        delta[j] <- m_delta[j] + s_delta * delta_raw[j];
    }
}

model {
    vector[N] p;	

    for (n in 1:N) {
        p[n] <- eta[jj[n]] + (1 - eta[jj[n]]) *
            inv_logit(exp(alpha[jj[n]]) *
                      (theta[ii[n]] - delta[jj[n]]));
   }

    theta_raw ~ normal(0, 1);
    alpha_raw ~ normal(0, 1);
    delta_raw ~ normal(0, 1);
    eta ~ beta(1, b);
    y ~ bernoulli(p);
}

generated quantities {
    vector[Q] diff;
    vector[K] m_thetas;
    vector[K] s_thetas;

    m_thetas <- rep_vector(0.0, K);
    s_thetas <- rep_vector(0.0, K);
    for (k in 1:K) {
        real thetas[T];
        int n;
        thetas <- rep_array(0.0, T);
        n <- 0;
        for (t in 1:T) {
            if (kt[t] == k) {
                n <- n + 1;
                thetas[n] <- theta[k];
            }
        }
        m_thetas[k] <- mean(segment(thetas, 1, n));
        s_thetas[k] <- sd(segment(thetas, 1, n));
    }
    for (j in 1:Q)
        diff[j] <- m_thetas[kq[j]] - delta[j];
}
