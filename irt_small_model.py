import pystan
from read import Q, K, DATA, RESULTS, get_difficulty
import pickle
from hashlib import md5

Q = list(Q)[:]
Q.sort()

obs = {}
obs['K'] = len(K)
obs['Q'] = len(Q)
obs['N'] = len(DATA)
obs['y'] = [int(r.success) for r in DATA]
# stan arrays are 1-based
obs['ii'] = [K.index(r.category)+1 for r in DATA]
obs['jj'] = [Q.index(r.quiz)+1 for r in DATA]
obs['kk'] = [K.index(RESULTS[q]['category'])+1 for q in Q]
# same order as Q
obs['m_step'] = []
for q in Q:
    if get_difficulty(q, RESULTS) == 'e':
        obs['m_step'].append(-1)
    elif get_difficulty(q, RESULTS) == 'm':
        obs['m_step'].append(0)
    else:  # hard
        obs['m_step'].append(1)

obs['b'] = []
for c in [RESULTS[q]['parms']['c'] for q in Q]:
    if c != 0:
        obs['b'].append((1/c) - 1)
    else:
        obs['b'].append(1000)


pystan.misc.stan_rdump(obs, 'kangourou-small.data.R')

code_hash = md5()
with open('irt-small.stan') as m:
    for r in m:
        code_hash.update(r.encode('ascii'))

code_hash = code_hash.hexdigest()

model = None
try:
    model = pickle.load(open('model-3PL-small-%s.pkl' % code_hash))
except:
    model = pystan.StanModel(file='irt-small.stan')
    pickle.dump(model,
                open('model-3PL-small-%s.pkl' % code_hash, 'w'))
