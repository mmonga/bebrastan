from collections import namedtuple


def read_data_flat():
    """Read results as a list of named tuples (team, school, pv, reg,
    quiz, success, category, time, maxr, score).

    """
    qs = []
    Answer = namedtuple('Answer', ['team', 'school', 'pv', 'reg', 'quiz',
                                   'success', 'category', 'time', 'maxr',
                                   'score'])
    with open('rank.csv') as data:
        for r in data:
            r = r.split(',')
            # first quiz in column 12: name, score, max, time
            c = 12
            while c + 3 < len(r):
                q = Answer(team=r[0],
                           school=r[5],
                           pv=r[9],
                           reg=r[10],
                           quiz=r[c],
                           category=r[1],
                           # success is getting the max score
                           success=float(r[c+1]) == float(r[c+2]),
                           time=int(r[c+3]),
                           maxr=float(r[c+2]),
                           score=float(r[c+1]))
                qs.append(q)
                c += 4
    return qs


def get_results_by_quiz(data):
    """Returns a dictionary with quiz names as keys and dictionary values
    (successes, minsolvet, maxr, tott, scoresum, parms, category)

    """
    qs = {}
    for r in data:
        t = dict.fromkeys(['successes', 'minsolvet', 'maxr', 'tott',
                           'scoresum', 'parms', 'category'])
        t['successes'] = []
        t['minsolvet'] = 2700*1000
        t['tott'] = 0
        t['maxr'] = r.maxr
        t['scoresum'] = 0
        t['parms'] = {}
        qs.setdefault(r.quiz, t)['successes'].append(r.success)
        if r.success and r.time < qs[r.quiz]['minsolvet']:
            qs[r.quiz]['minsolvet'] = r.time
        qs[r.quiz]['scoresum'] += r.score
        qs[r.quiz]['category'] = r.category
        qs[r.quiz]['tott'] += r.time
    return qs


def get_difficulty(qname, results):
    """Returns 'e', 'm', or 'h' according to authors' rating

    """
    if results[qname]['maxr'] == 6.0:
        return 'h'
    elif results[qname]['maxr'] == 2.0:
        return 'e'
    else:
        return 'm'


def get_results_by_category(data):
    """Returns a dictionary with categories as keys and quiz
    dictionaries as values

    """
    results = {}
    qq = get_results_by_quiz(data)
    for q, v in qq.items():
        category = v['category']
        results.setdefault(category, {}).setdefault(q, v)
    return results

DATA = read_data_flat()
MINP = {'Benjamin_2014-SK-07': 1./8,
        'Benjamin_2014-CZ-02a+': 1./4,
        'Benjamin_2014-CA-07+': 1./(7*6*5*4*3*2),
        'Benjamin_2014-JP-05a': 1./4,
        'Benjamin_2014-CZ-08': 1./4,
        'Benjamin_2014-AU-03a+': 0.0,
        'Benjamin_2013-BE-16b': 1./4.,
        'Benjamin_2014-JP-03': 1./8,
        'Benjamin_2014-FR-04*': 0.0,
        'Benjamin_2014-SE-04': 1./4,
        'Benjamin_2014-DE-04': 1./4,
        'Benjamin_2014-CH-02': 1./(5*4*3*2),
        'Benjamin_2014-CA-05': 1./4,
        'Benjamin_2014-RU-03': 1./16,
        'Benjamin_Disegni sullo schermo': 1./4,
        'Benjamin_2011-CH-06+': 0.0,
        'Cadet_2014-SK-07': 1./8,
        'Cadet_2014-CZ-02a+': 1./4,
        'Cadet_2014-CA-07+': 1./(7*6*5*4*3*2),
        'Cadet_2014-FR-01': 1./(8*7*6*5*4*3*2),
        'Cadet_2014-JP-05a': 1./4,
        'Cadet_2014-FI-04': 1./4,
        'Cadet_Crucintarsio': 0.0,
        'Cadet_2014-AU-03a+': 0.0,
        'Cadet_2014-SE-04': 1/4.,
        'Cadet_2014-DE-04': 1./4,
        'Cadet_2014-HU-02': 1./4,
        'Cadet_2014-CH-02': 1./(5*4*3*2),
        'Cadet_2014-RU-03': 1./16,
        'Cadet_2014-CH-07+': 0.0,
        'Cadet_Disegni sullo schermo': 1./4,
        'Cadet_2014-PL-07+': 0.0,
        'Cadet_2011-CH-06+': 0.0,
        'Junior_2014-CH-06*': 0.0,
        'Junior_2014-SI-04': 1./4,
        'Junior_2014-CZ-02a+': 1./4,
        'Junior_2014-CA-01*': 0.0,
        'Junior_2014-FR-01': 1./(8*7*6*5*4*3*2),
        'Junior_2014-FI-04': 1./4,
        'Junior_Crucintarsio': 0.0,
        'Junior_2014-AU-03a+': 0.0,
        'Junior_2014-HU-02': 1./4,
        'Junior_2014-CH-02': 1./(5*4*3*2),
        'Junior_2014-RU-03': 1./16,
        'Junior_2014-CH-07+': 0.0,
        'Junior_Disegni sullo schermo': 1./4,
        'Junior_2014-PL-07+': 0.0,
        'Junior_2011-CH-06+': 0.0,
        'Junior_2011-DE-09*': 0.0,
        'Junior_2014-RU-06+': 0.0,
        'Student_2014-CH-06*': 0.0,
        'Student_2014-SI-04': 1./4,
        'Student_2014-CZ-02a+': 1./4,
        'Student_2014-CA-01*': 0.0,
        'Student_2014-FR-01': 1./(8*7*6*5*4*3*2),
        'Student_2014-FI-04': 1./4,
        'Student_Crittografia': 0.0,
        'Student_Crucintarsio': 0.0,
        'Student_2013-BE-15a': 1./4,
        'Student_2014-IT-05+': 1./40,
        'Student_2014-TW-04+': 0.0,
        'Student_2014-HU-02': 1./4,
        'Student_2014-CH-07+': 0.0,
        'Student_2013-FR-05': 0.0,
        'Student_2014-PL-07+': 0.0,
        'Student_2011-DE-09*': 0.0,
        'Student_2014-RU-06+': 0.0
        }
RESULTS = get_results_by_quiz(DATA)
KRESULTS = get_results_by_category(DATA)
for i in MINP:
    RESULTS[i]['parms']['c'] = MINP[i]
    k = RESULTS[i]['category']
    KRESULTS[k][i]['parms']['c'] = MINP[i]
T = set([r.team for r in DATA])  # teams
Q = set([r.quiz for r in DATA])  # quiz names
K = KRESULTS.keys()[:]
